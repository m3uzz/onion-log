<?php
/**
 * This file is part of Onion
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    Onion
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-zf
 */
declare (strict_types = 1);

namespace OnionLog;
use OnionLib\Util;


class EventLog extends AbstractLog implements LogInterface
{
	/**
	 * 
	 * @var \OnionLog\EventLog
	 */
	private static $oInstance;

	
	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }


	/**
	 * 
	 */
    public static function getInstance (array $paConfigLog = [], array $paDbConnection = []) : EventLog
    {
		if(self::$oInstance === null)
		{
            self::$oInstance = new self($paConfigLog, $paDbConnection);
		}
		
        return self::$oInstance;
	}


    /**
     * System is unusable.
     *
     * @param array $paMsg
     * @return void
     */
    public function emergency (array $paMsg) : void
	{
		$this->save($paMsg, LogLevel::EMERGENCY);
	}


    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param array $paMsg
     * @return void
     */
    public function alert (array $paMsg) : void
	{
		$this->save($paMsg, LogLevel::ALERT);
	}


    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param array $paMsg
     * @return void
     */
    public function critical (array $paMsg) : void
	{
		$this->save($paMsg, LogLevel::CRITICAL);	
	}


    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param array $paMsg
     * @return void
     */
    public function error (array $paMsg) : void
	{
		$this->save($paMsg, LogLevel::ERROR);
	}


    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param array $paMsg
     * @return void
     */
    public function warning (array $paMsg) : void
	{
		$this->save($paMsg, LogLevel::WARNING);
	}


    /**
     * Normal but significant events.
     *
     * @param array $paMsg
     * @return void
     */
    public function notice (array $paMsg) : void
	{
		$this->save($paMsg, LogLevel::NOTICE);
	}


    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param array $paMsg
     * @return void
     */
    public function info (array $paMsg) : void
	{
		$this->save($paMsg, LogLevel::INFO);
	}


    /**
     * Detailed debug information.
     *
     * @param array $paMsg
     * @return void
     */
	public function debug (array $paMsg) : void
	{
		$this->save($paMsg, LogLevel::DEBUG);
	}
	

	/**
	 * Event log register
	 * 
	 * @param array $paMsg The message need to be an array with any value
	 */
	public function save (array $paMsg, ?string $psLevel = null, ?string $pcOutput = null, bool $pbSave = false) : void
	{
		$this->setConfigType(Type::EVENTS);
		
		if (Util::toBoolean($this->bLogEnable) || $pbSave)
		{
			$lsOutput = $pcOutput === null ? $this->sLogOutput : $pcOutput;
	
			$lsMsg = json_encode($paMsg, JSON_UNESCAPED_UNICODE);

            if ($lsOutput == Output::DB)
			{
                $laData['SysAccount_id'] = $this->nAccountId;
				$laData['SysUserOwner_id'] = $this->nUserId;
				$laData['stPriority'] = $psLevel === null ? LogLevel::INFO : $psLevel;
				$laData['stIP'] = (isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ''));
                $laData['stResource'] = isset($this->aResource['resource']) ? $this->aResource['resource'] : null;
                $laData['stAction'] = isset($this->aResource['action']) ? $this->aResource['action'] : null;
                $laData['Resource_id'] = isset($this->aResource['id']) ? $this->aResource['id'] : null;
                $laData['Resource_hash'] = isset($this->aResource['hash']) ? $this->aResource['hash'] : null;
				$laData['stMessage'] = $lsMsg;
				$laData['txtServer'] = json_encode($_SERVER);
				
				$this->logDb($laData);
			}
			else 
			{
				$_SERVER['X-Onion-Event-UserId'] = $this->nUserId;
				$_SERVER['X-Onion-Event-Priority'] = $psLevel === null ? LogLevel::INFO : $psLevel;
				$_SERVER['X-Onion-Event-Msg'] = $lsMsg;
				
				$this->logStream($_SERVER);
			}
		}
	}
}