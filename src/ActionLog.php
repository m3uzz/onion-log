<?php
/**
 * This file is part of Onion
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    Onion
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-zf
 */
declare (strict_types = 1);

namespace OnionLog;
use OnionLib\Util;


class ActionLog extends AbstractLog implements LogInterface
{
	/**
	 * 
	 * @var \OnionLog\ActionLog
	 */
	private static $oInstance;

	
	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }


	/**
	 * 
	 */
    public static function getInstance (array $paConfigLog = [], array $paDbConnection = []) : ActionLog
    {
		if(self::$oInstance === null)
		{
            self::$oInstance = new self($paConfigLog, $paDbConnection);
		}
		
        return self::$oInstance;
	}
	
	
	/**
	 * Click action register
     * 
	 * @param string $psUrl The url clicked
	 * @param string|null $psResource The table or module name to group
	 * @param string|null $psId The id of the item 
	 */
	public function saveClick (string $psUrl, ?string $psResource = null, ?string $psId = null) : void
	{
		$this->setConfigType(Type::CLICK);
		$this->save(['url' => $psUrl, 'resource' => $psResource, 'id' => $psId]);
	}

	
	/**
	 * View action register
     * 
	 * @param string $psUrl The url clicked
	 * @param string|null $psResource The table or module name to group
	 * @param string|null $psId The id of the item 
	 */
	public function saveView (string $psUrl, ?string $psResource = null, ?string $psId = null) : void
	{
		$this->setConfigType(Type::VIEW);
		$this->save(['url' => $psUrl, 'resource' => $psResource, 'id' => $psId]);
	}

	
	/**
	 * Action log register
	 * 
	 * @param array $paMsg The message need to be an array containing three elements: 
	 * ```
	 * ['url' => string, 'resource' => string, 'id' => string]
	 * ```
	 */
	public function save (array $paMsg, ?string $psLevel = null, ?string $pcOutput = null, bool $pbSave = false) : void
	{
		if (Util::toBoolean($this->bLogEnable) || $pbSave)
		{
			$lsOutput = $pcOutput === null ? $this->sLogOutput : $pcOutput;
	
			if ($lsOutput == Output::DB)
			{
				$laData['SysUserOwner_id'] = $this->nUserId;
				$laData['stPriority'] = $psLevel === null ? LogLevel::INFO : $psLevel;
				$laData['stUrl'] = isset($paMsg['url']) ? $paMsg['url'] : '';
				$laData['stResource'] = isset($paMsg['resource']) ? $paMsg['resource'] : '';
				$laData['Resource_Id'] = isset($paMsg['id']) ? $paMsg['id'] : '';
				$laData['txtServer'] = json_encode($_SERVER);
				
				$this->logDb($laData);
			}
			else
			{
				$_SERVER['X-Onion-Action-UserId'] = $this->nUserId;
				$_SERVER['X-Onion-Action-Priority'] = $psLevel === null ? LogLevel::INFO : $psLevel;
				$_SERVER['X-Onion-Action-Url'] = isset($paMsg['url']) ? $paMsg['url'] : '';
				$_SERVER['X-Onion-Action-Resource'] = isset($paMsg['resource']) ? $paMsg['resource'] : '';
				$_SERVER['X-Onion-Action-ResourceId'] = isset($paMsg['id']) ? $paMsg['id'] : '';
				
				$this->logStream($_SERVER);
			}
		}
	}
}