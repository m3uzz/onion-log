<?php
/**
 * This file is part of Onion Log
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLog
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-log
 */
declare (strict_types = 1);

namespace OnionLog;
use OnionLib\Util;


class AccessLog extends AbstractLog implements LogInterface
{
	/**
	 * 
	 * @var \OnionLog\AccessLog
	 */
	private static $oInstance;
	
	
	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }


	/**
	 * 
	 */
    public static function getInstance (array $paConfigLog = [], array $paDbConnection = []) : AccessLog
    {
		if(self::$oInstance === null)
		{
            self::$oInstance = new self($paConfigLog, $paDbConnection);
		}
		
        return self::$oInstance;
	}


	/**
	 * Access log register
	 * 
	 * @param string $psMsg
	 */
	public function saveMsg (string $psMsg) : void
	{
		$this->save(['msg' => $psMsg]);
	}


	/**
	 * Access log register
	 * 
	 * @param array $paMsg The message need to be an array containing one element: 
	 * ```
	 * ['msg' => string]
	 * ```
	 */
	public function save (array $paMsg, ?string $psLevel = null, ?string $pcOutput = null, bool $pbSave = false) : void
	{
		$this->setConfigType(Type::ACCESS);

		if (Util::toBoolean($this->bLogEnable) || $pbSave)
		{
			$lsOutput = $pcOutput === null ? $this->sLogOutput : $pcOutput;

			if (is_null($this->sSessionId))
			{
				$this->sSessionId = session_id();
			}

			if ($lsOutput == Output::DB)
			{
				$laData['SysAccount_id'] = $this->nAccountId;
				$laData['SysUser_id'] = $this->nUserId;
				$laData['stUserName'] = $this->sUserName;
				$laData['SysUserGroup_id'] = $this->nUserGroupId;
				$laData['stPriority'] = $psLevel === null ? LogLevel::INFO : $psLevel;
				$laData['stSession'] = $this->sSessionId;
				$laData['stIP'] = (isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ''));
				$laData['stMessage'] = isset($paMsg['msg']) ? $paMsg['msg'] : '';
				$laData['stUserAgent'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';

				$laServer = $_SERVER;
				$laServer['ONION_VHOSTIP'] = $this->aVHostIp;
				$laServer['ONION_CONTEXT'] = $this->aContext;
				$laData['txtServer'] = json_encode($laServer);
				
				$this->logDb($laData);
			}
			else
			{
				$_SERVER['X-Onion-Access-AccountId'] = $this->nAccountId;
				$_SERVER['X-Onion-Access-UserId'] = $this->nUserId;
				$_SERVER['X-Onion-Access-UserName'] = $this->sUserName;
				$_SERVER['X-Onion-Access-UserGroupId'] = $this->nUserGroupId;
				$_SERVER['X-Onion-Access-Session'] = $this->sSessionId;
				$_SERVER['X-Onion-Access-Priority'] = $psLevel === null ? LogLevel::INFO : $psLevel;
				$_SERVER['X-Onion-Access-Msg'] = isset($paMsg['msg']) ? $paMsg['msg'] : '';
				
				$this->logStream($_SERVER);
			}
		}
	}
}