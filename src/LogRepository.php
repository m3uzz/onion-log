<?php
/**
 * This file is part of Onion 
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLog
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-log
 */
declare (strict_types = 1);

namespace OnionLog;
use OnionDb\AbstractRepository;
use OnionLib\Str;


class LogRepository extends AbstractRepository
{
	/**
	 * 
	 * @var \OnionLog\LogRepository
	 */
	private static $oInstance;

	
	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }


	/**
	 * Get the repository singleton instance
	 * 
	 * @param array $paConf The database configurarion
	 * ```
	 * $paConf = [ // Params to connect into database
	 *   'driver'	=> string, // Database connection driver, ex.: PDOMySql
	 *   'database' => string, // Database name,
	 *   'username' => string, // Database username,
	 *   'password' => string, // Database password,
	 *   'hostname' => string, // Database hostname, ex.: localhost
	 *   'port' => string, // Database port connection, ex.: 3306
	 *   'charset' => string, // Database charset, ex.: UTF8,
	 * ];
	 * ```
	 * @return \OnionLog\LogRepository
	 */
    public static function getInstance (array $paConf = []) : LogRepository
    {
		if(self::$oInstance === null)
		{
            self::$oInstance = new self($paConf);
		}
		
        return self::$oInstance;
	}
	
	
	/**
	 * Prepare data to insert int table
	 * 
	 * @param array $paData
	 * ```
	 * $paData = [
	 *   'fieldName1' => 'value',
	 *   'fieldName2' => 'value',
	 *   ...
	 * ]
	 * ``` 
	 * @return array
	 */
	private function prepare (array $paData) : array
	{
		$lsFields = '';
		$lsValues = '';
		$lsComma = '';
		
		if (is_array($paData))
		{
			foreach ($paData as $lsField => $lsValue)
			{
				if (!empty($lsValue) || $lsValue == "0")
				{
					$lsFields .= $lsComma . "`{$lsField}`";
					$lsValue = Str::escapeString($lsValue);
					$lsValues .= $lsComma . "'{$lsValue}'";
					$lsComma = ", ";
				}
			}
		}
		
		return ['fields' => $lsFields, 'values' => $lsValues];
	}
	
	
	/**
	 * Save data into data base table
	 * 
	 * @param array $paData
	 * ```
	 * $paData = [
	 *   'fieldName1' => 'value',
	 *   'fieldName2' => 'value',
	 *   ...
	 * ]
	 * ```
	 * @param string $psTable The table name to save data
	 * @return bool Return true if insert ok or false if fail
	 */
	public function saveData (array $paData, string $psTable)
	{
		if (defined('APP_MOD_DEMO') && APP_MOD_DEMO)
		{
			return true;
		}

		$lsSlug = Str::slugfy($psTable);
		$lsToHash = $psTable . microtime();
		$paData['stHash'] = "{$lsSlug}_" . md5($lsToHash);
		$paData['dtInsert'] = date('Y-m-d H:i:s');
		$paData['dtUpdate'] = date('Y-m-d H:i:s');
		$paData['isActive'] = '1';
		
		$paData = $this->prepare($paData);
		
		$lsSql = "INSERT IGNORE INTO {$psTable}
		({$paData['fields']})
		VALUES
		({$paData['values']})
		";
		
		return $this->insert($lsSql);
	}
}