<?php
/**
 * This file is part of Onion Log
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLog
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-log
 */
declare (strict_types = 1);

namespace OnionLog;
use OnionLib\System;


defined('DS') || define('DS', DIRECTORY_SEPARATOR);

class Type {
    const ACCESS = 'access';
    const CLICK  = 'click';
    const EVENTS = 'events';
    const SEARCH = 'search';
    const VIEW   = 'view';
}


class Output {
    const DB     = 'DB';
    const STREAM = 'STREAM';
}


/**
 * Describes log levels.
 */
class LogLevel
{
    const EMERGENCY = 'emergency';  // Emergency: system is unusable
    const ALERT     = 'alert';  // Alert: action must be taken immediately
    const CRITICAL  = 'critical';  // Critical: critical conditions
    const ERROR     = 'error';  // Error: error conditions
    const WARNING   = 'warning';  // Warning: warning conditions
    const NOTICE    = 'notice';  // Notice: normal but significant condition
    const INFO      = 'info';  // Informational: informational messages
    const DEBUG     = 'debug';  // Debug: Debug messages
}


abstract class AbstractLog
{
	/**
	 * @var int
	 */
	protected $nAccountId = null;

	/**
	 * @var int
	 */
	protected $nUserId = null;
	
	/**
	 * @var string
	 */
	protected $sUserName = null;
	
	/**
	 * @var int
	 */
	protected $nUserGroupId = null;

	/**
	 * @var array
	 */
	protected $aContext = [];
	
	/**
	 * @var array
	 */
	protected $aVHostIp = [];

	/**
	 * @var array
	 */
	protected $aResource = [
		'resource' => null,
		'action' => null,
		'id' => null,
		'hash' => null,
	];

	/**
	 * @var array
	 */
	protected $aConfigLog = [];

	/**
	 * @var array
	 */
	protected $aConfigDb = [];
	
	/**
	 * @var bool
	 */
	protected $bLogEnable = false;
	
	/**
	 * @var string
	 */
	protected $sLogOutput = null;
	
	/**
	 * @var string
	 */
	protected $sLogTable = null;
	
	/**
	 * @var string
	 */
	protected $sLogFileName = null;
	
	/**
	 * @var string
	 */
	protected $sLogPath = null;
	
	/**
	 * @var string
	 */
	protected $sSessionId = null;
	
	/**
	 * @var string
	 */
	protected $sSeparator = "\t";
	

    /**
     * 
	 * @param array $paConfigLog An array with log config
     * ```
     * $paConfigLog = [
	 *	 'TYPE' => [ // access | action | events | click | search | view
     *     'enable' => bool,
	 *	   'output' => string, // DB | STREAM
	 *	   'table' => string,  // Database table name to save data log if output was DB
     *	   'fileName' => string, // File name to save data log if output was STREAM
     *   ],
     *   'logDir' => string, // Path to save log file if output was STREAM
     * ];
	 * ```
	 * @param array $paDbConnection Params to connect into database if output was STREAM
	 * ```
     *   'db' => [
	 *     'driver'	=> string, // Database connection driver, ex.: PDOMySql
	 *     'database' => string, // Database name,
	 *     'username' => string, // Database username,
	 *     'password' => string, // Database password,
	 *     'hostname' => string, // Database hostname, ex.: localhost
	 *     'port' => string, // Database port connection, ex.: 3306
	 *     'charset' => string, // Database charset, ex.: UTF8,
	 *   ],
     * ];
     * ```
     */
	protected function __construct (array $paConfigLog = [], array $paDbConnection = [])
	{
		$this->aConfigLog = $paConfigLog;
		$this->aConfigDb = $paDbConnection;
	}
	
	
	/**
	 *
	 * @param string $psProperty
	 * @param bool|string|array $pmValue
	 * @throws \Exception
	 */
	public function set (string $psProperty, $pmValue) : void 
	{
		if (property_exists($this, $psProperty))
		{
			$lsMethod = 'set'.$psProperty;
			
			if (method_exists($this, $lsMethod))
			{
				$this->$lsMethod($pmValue);
			}
			else
			{
				throw new \Exception("Method {$lsMethod} does not exist!");
			}
		}
		else
		{
			throw new \Exception("Property {$psProperty} does not exist!");
		}
	}

	
	/**
	 *
	 * @param string $psType access | events | click | search | view
	 */
	public function setConfigType (string $psType) : AbstractLog
	{
		$this->sLogPath = $this->aConfigLog['logDir'];
		$this->bLogEnable = $this->aConfigLog[$psType]['enable'];
		$this->sLogOutput = $this->aConfigLog[$psType]['output'];
		$this->sLogTable = $this->aConfigLog[$psType]['table'];
		$this->sLogFileName = $this->aConfigLog[$psType]['fileName'];
		
		return $this;
	}
	

	/**
	 * 
	 * @param array $paConfigLog An array with log config
	 * ```
	 * $paConfigLog = [
	 *	 'TYPE' => [ // access | events | click | search | view
	 *     'enable' => bool,
	 *	   'output' => string, // 'DB' | 'STREAM'
	 *	   'table' => string,  // Database table name to save data log if output was DB
	 *	   'fileName' => string, // File name to save data log if output was STREAM
	 *   ],
	 *   'logDir' => string, // Path to save log file if output was STREAM
	 * ];
	 * ```
	 */
	public function setConfigLog (array $paConfigLog = []) : AbstractLog
	{
		$this->aConfigLog = $paConfigLog;
		
		return $this;
	}

	
    /**
     * 
	 * ```
	 * @param array $paDbConnection Params to connect into database if output was STREAM
	 * ```
     *   'db' => [
	 *     'driver'	=> string, // Database connection driver, ex.: PDOMySql
	 *     'database' => string, // Database name,
	 *     'username' => string, // Database username,
	 *     'password' => string, // Database password,
	 *     'hostname' => string, // Database hostname, ex.: localhost
	 *     'port' => string, // Database port connection, ex.: 3306
	 *     'charset' => string, // Database charset, ex.: UTF8,
	 *   ],
     * ];
     * ```
     */
	public function setDbConnection (array $paDbConnection = []) : AbstractLog
	{
		$this->aConfigDb = $paDbConnection;
		
		return $this;
	}

	
	/**
	 *
	 * @param array $paUser The user identification
	 * ```
     * setUser([
	 * 	 'AccountId => int,
     *   'UserId' => int,
     *   'UserName' => string,
     *   'UserGroupId' => int
	 *   'Context' => array
	 *   'VHostIp' => array
     * ]);
     * ```
	 */
	public function setUser (array $paUser) : AbstractLog
	{
		if (is_array($paUser))
		{
			if (isset($paUser['AccountId']))
			{
				$this->nAccountId = $paUser['AccountId'];
			}

			if (isset($paUser['UserId']))
			{
				$this->nUserId = $paUser['UserId'];
			}
			
			if (isset($paUser['UserName']))
			{
				$this->sUserName = $paUser['UserName'];
			}
			
			if (isset($paUser['UserGroupId']))
			{
				$this->nUserGroupId = $paUser['UserGroupId'];
			}
						
			if (isset($paUser['Context']))
			{
				$this->aContext = $paUser['Context'];
			}
						
			if (isset($paUser['VHostIp']))
			{
				$this->aVHostIp = $paUser['VHostIp'];
			}		
		}
		
		return $this;
	}
		

	/**
	 *
	 * @param array $paResource
	 * ```
     * setResource([
	 * 	 'resource' => string,
     *   'action' => string,
     *   'id' => int,
     *   'hash' => string
     * ]);
	 * ```
	 */
	public function setResource (array $paResource) : AbstractLog
	{
		$this->aResource = $paResource;
		
		return $this;
	}


	/**
	 *
	 * @param string $psSessionId
	 */
	public function setSessionId (string $psSessionId) : AbstractLog
	{
		$this->sSessionId = $psSessionId;
		
		return $this;
	}
		

	/**
	 * Register the log data in a data base
	 * 
	 * @param array $paData
	 */
	public function logDb (array $paData) : void
	{
		$loDb = LogRepository::getInstance($this->aConfigDb);
		$loDb->saveData($paData, $this->sLogTable);
	}

	
	/**
	 * Register the log data in a system file
	 *
	 * @param array $paData
	 * @param string|null $psOutput Override the global config to force the postfix file name
	 */
	public function logStream (array $paData, ?string $psOutput = null) : void
	{
		$lsLine = "";
		
		if (is_array($paData))
		{
			$lsSeparator = "";
			
			foreach ($paData as $lsKey => $lsItem)
			{
				$lsLine .= $lsSeparator . $lsItem;
				$lsSeparator = $this->sSeparator;
			}
		}
		
		$lsFilePosfix = "_" . $this->sLogFileName;
		
		if ($psOutput != null)
		{
			$lsFilePosfix = "_" . $psOutput;
		}
		
		$lsFileLog = $this->sLogPath. DS . date("Y-m-d") . $lsFilePosfix . ".log";
		
		if (file_exists($lsFileLog))
		{
			System::saveFile($lsFileLog, $lsLine, "APPEND");
		}
		else 
		{
			System::saveFile($lsFileLog, $lsLine);
		}
	}
	
	
	/**
	 *
	 * @param string $psSource
	 * @return array
	 */
	public function getLog (string $psSource) : array
	{
		//TODO: create a parse log;
		return [];
	}
	
	
	/**
	 *
	 * @return array
	 */
	public function getLogStream () : array
	{
		return $this->getLog(Output::STREAM);
	}
	
	
	/**
	 *
	 * @return array
	 */
	public function getLogDb () : array
	{
		return $this->getLog(Output::DB);
	}
}