<?php
/**
 * This file is part of Onion Log
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionLog
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-log
 */
declare (strict_types = 1);

namespace OnionLog;


interface LogInterface
{
    /**
     * Get the log singleton instance
     * 
	 * @param array $paConfigLog An array with log config
     * ```
     * $paConfigLog = [
	 *	 'TYPE' => [ // access | action | events | click | search | view
     *     'enable' => bool,
	 *	   'output' => string, // DB | STREAM
	 *	   'table' => string,  // Database table name to save data log if output was DB
     *	   'fileName' => string, // File name to save data log if output was STREAM
     *   ],
     *   'logDir' => string, // Path to save log file if output was STREAM
     * ];
	 * ```
	 * @param array $paDbConnection Params to connect into database if output was STREAM
	 * ```
     *   'db' => [
	 *     'driver'	=> string, // Database connection driver, ex.: PDOMySql
	 *     'database' => string, // Database name,
	 *     'username' => string, // Database username,
	 *     'password' => string, // Database password,
	 *     'hostname' => string, // Database hostname, ex.: localhost
	 *     'port' => string, // Database port connection, ex.: 3306
	 *     'charset' => string, // Database charset, ex.: UTF8,
	 *   ],
     * ];
     * ```
     */
    public static function getInstance (array $paConfigLog = [], array $paDbConnection = []) : LogInterface;
    

    /**
     *
	 * @param array $paMsg
	 * @param string|null $psLevel The message priority `EMERGENCY` | `ALERT` | `CRITICAL` | `ERROR` | `WARNING` | `NOTICE` | `INFO` | `DEBUG`
	 * @param string|null $psOutput Override the global config to force the output type: `DB` | `STREAM`
	 * @param bool $pbSave Override the global config to force registre log
     */
    public function save (array $paMsg, ?string $psLevel = null, ?string $psOutput = null, bool $pbSave = false) : void;
}