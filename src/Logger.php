<?php
/**
 * This file is part of Onion
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    Onion
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-zf
 */
declare (strict_types = 1);

namespace OnionLog;
use OnionLib\Util;


class Logger extends AbstractLog implements LogInterface
{
	/**
	 * 
	 * @var \OnionLog\Logger
	 */
	private static $oInstance;

	
	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }


	/**
	 * 
	 */
    public static function getInstance (array $paConfigLog = [], array $paDbConnection = []) : Logger
    {
		if(self::$oInstance === null)
		{
            self::$oInstance = new self($paConfigLog, $paDbConnection);
		}
		
        return self::$oInstance;
	}


    /**
     * System is unusable.
     *
     * @param string $psMsg
     * @param array $paContext
     * @return void
     */
    public function emergency (string $psMsg, array $paContext = []) : void
	{
        $this->log(LogLevel::EMERGENCY, $psMsg, $paContext);
	}


    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $psMsg
     * @param array $paContext
     * @return void
     */
    public function alert (string $psMsg, array $paContext = []) : void
	{
        $this->log(LogLevel::ALERT, $psMsg, $paContext);
	}


    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $psMsg
     * @param array $paContext
     * @return void
     */
    public function critical (string $psMsg, array $paContext = []) : void
	{
        $this->log(LogLevel::CRITICAL, $psMsg, $paContext);
	}


    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $psMsg
     * @param array $paContext
     * @return void
     */
    public function error (string $psMsg, array $paContext = []) : void
	{
        $this->log(LogLevel::ERROR, $psMsg, $paContext);
	}


    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $psMsg
     * @param array $paContext
     * @return void
     */
    public function warning (string $psMsg, array $paContext = []) : void
	{
        $this->log(LogLevel::WARNING, $psMsg, $paContext);
	}


    /**
     * Normal but significant events.
     *
     * @param string $psMsg
     * @param array $paContext
     * @return void
     */
    public function notice (string $psMsg, array $paContext = []) : void
	{
        $this->log(LogLevel::NOTICE, $psMsg, $paContext);
	}


    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $psMsg
     * @param array $paContext
     * @return void
     */
    public function info (string $psMsg, array $paContext = []) : void
	{
        $this->log(LogLevel::INFO, $psMsg, $paContext);
	}


    /**
     * Detailed debug information.
     *
     * @param string $psMsg
     * @param array $paContext
     * @return void
     */
	public function debug (string $psMsg, array $paContext = []) : void
	{
		$this->log(LogLevel::DEBUG, $psMsg, $paContext);
	}
	

    /**
     * Logs with an arbitrary level.
     *
     * @param string $psLevel LogLevel const
     * @param string $psMsg
     * @param array $paContext
     * @return void
     */
    public function log (string $psLevel, string $psMsg, array $paContext = []) : void
    {
        $laMsg['message'] = $this->interpolate($psMsg, $paContext);
        $this->save($laMsg, $psLevel);
    }


    /**
     * Interpolates context values into the message placeholders.
     * @param string $psMsg
     * @param array $paContext
     * @return string
     */
    private function interpolate (string $psMsg, array $paContext = []) : string
    {
        // build a replacement array with braces around the context keys
        $laReplace = [];

        foreach ($paContext as $lsKey => $lsVal) 
        {
            // check that the value can be cast to string
            if (!is_array($lsVal) && (!is_object($lsVal) || method_exists($lsVal, '__toString'))) 
            {
                $laReplace['{' . $lsKey . '}'] = $lsVal;
            }
        }

        // interpolate replacement values into the message and return
        return strtr($psMsg, $laReplace);
    }


	/**
	 * Event log register
	 * 
	 * @param array $paMsg The message need to be an array with any value
	 */
	public function save (array $paMsg, ?string $psLevel = null, ?string $pcOutput = null, bool $pbSave = false) : void
	{
		$this->setConfigType(Type::EVENTS);
		
		if (Util::toBoolean($this->bLogEnable) || $pbSave)
		{
			$lsOutput = $pcOutput === null ? $this->sLogOutput : $pcOutput;
			
			if ($lsOutput == Output::DB)
			{
				$laData['SysUserOwner_id'] = $this->nUserId;
				$laData['stPriority'] = $psLevel === null ? LogLevel::INFO : $psLevel;
				$laData['stIP'] = (isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ''));
				$laData['stMessage'] = isset($paMsg['message']) ? $paMsg['message'] : '';
				$laData['txtServer'] = json_encode($_SERVER);
				
				$this->logDb($laData);
			}
			else 
			{
				$_SERVER['X-Onion-Event-UserId'] = $this->nUserId;
				$_SERVER['X-Onion-Event-Priority'] = $psLevel === null ? LogLevel::INFO : $psLevel;
				$_SERVER['X-Onion-Event-Msg'] = isset($paMsg['message']) ? $paMsg['message'] : '';
				
				$this->logStream($_SERVER);
			}
		}
	}
}